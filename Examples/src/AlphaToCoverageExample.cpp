#include "AlphaToCoverageExample.h"

#include "WinKeyMap.h"

#include "imgui/imgui.h"

#include <directxcolors.h>


using namespace DirectX;

namespace {
void drawUI(AlphaToCoverage::BlendMode& mode) {
    ImGui::SetNextWindowBgAlpha(1.0f);
    ImGui::Begin("Alpha to coverage controls");

        ImGui::Text("Blend modes:");

        if (ImGui::RadioButton("Alpha to coverage", mode == AlphaToCoverage::AlphaToCoverage))
            mode = AlphaToCoverage::AlphaToCoverage;

        if (ImGui::RadioButton("Alpha blending", mode == AlphaToCoverage::Blend))
            mode = AlphaToCoverage::Blend;

        if (ImGui::RadioButton("No blending", mode == AlphaToCoverage::NoBlend))
            mode = AlphaToCoverage::NoBlend;

    ImGui::End();
}

}

namespace AlphaToCoverage {

HRESULT AlphaToCoverageExample::setup() {
    auto hr = BaseExample::setup();
    if (FAILED(hr))
        return hr;

    enableFreeflyMode(false);

    texture_ = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/AlphaToCoverage.dds", true);
    sampler_ = std::make_unique<AnisotropicSampler<D3D11_TEXTURE_ADDRESS_WRAP>>(context_.d3dDevice_);

    quad_ = std::make_unique<Quad>(context_.d3dDevice_);

    hr = reloadShaders();
    if (FAILED(hr))
        return hr;

    D3D11_BLEND_DESC blendDesc;
    ZeroMemory(&blendDesc, sizeof(D3D11_BLEND_DESC));
    blendDesc.AlphaToCoverageEnable = true;
    blendDesc.RenderTarget[0].BlendEnable = false;
    blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
    blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
    blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
    blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
    blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
    blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

    hr = context_.d3dDevice_->CreateBlendState(&blendDesc, alphaToCoverageBlendState_.GetAddressOf());
    if (FAILED(hr)) {
        assert(!"Failed to create blend state");
        return hr;
    }

    blendDesc.AlphaToCoverageEnable = false;
    blendDesc.RenderTarget[0].BlendEnable = true;
    hr = context_.d3dDevice_->CreateBlendState(&blendDesc, alphaBlendingBlendState_.GetAddressOf());
    if (FAILED(hr)) {
        assert(!"Failed to create blend state");
        return hr;
    }

    blendDesc.AlphaToCoverageEnable = false;
    blendDesc.RenderTarget[0].BlendEnable = false;
    hr = context_.d3dDevice_->CreateBlendState(&blendDesc, noBlendingBlendState_.GetAddressOf());
    if (FAILED(hr)) {
        assert(!"Failed to create blend state");
        return hr;
    }

    currentBlendState_ = alphaToCoverageBlendState_;

    return hr;
}

bool AlphaToCoverageExample::reloadShadersInternal() {
    return Shaders::makeShader<AtoCShader>(
        shader_,
        context_.d3dDevice_,
        "shaders/AlphaToCoverage.fx", "VS",
        "shaders/AlphaToCoverage.fx", "PS",
        quad_->getVertexLayout()
    );
}

void AlphaToCoverageExample::setupBlendState() {
    switch (blendMode_) {
        case AlphaToCoverage:
            currentBlendState_ = alphaToCoverageBlendState_;
            break;
        case Blend:
            currentBlendState_ = alphaBlendingBlendState_;
            break;
        case NoBlend:
            currentBlendState_ = noBlendingBlendState_;
            break;
        default:
            currentBlendState_ = alphaToCoverageBlendState_;
            break;
    }
}

void AlphaToCoverageExample::render() {
    BaseExample::render();

    setupBlendState();

    context_.immediateContext_->ClearRenderTargetView(context_.renderTargetView_, ex::srgbToLinear(DirectX::Colors::Red));
    context_.immediateContext_->ClearDepthStencilView(context_.depthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);

    float bl[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    context_.immediateContext_->OMSetBlendState(currentBlendState_.Get(), bl, 0xffffffff);

    shader_->use(context_.immediateContext_);
    texture_->use(context_.immediateContext_, 0);
    sampler_->use(context_.immediateContext_, 0);
    quad_->draw(context_);

    drawUI(blendMode_);

    present(0, 0);
}

}
