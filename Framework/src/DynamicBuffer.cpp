#include "DynamicBuffer.h"

#include <cassert>

D3D11_BIND_FLAG typeToBindFlag(BufferType type) {
    switch (type) {
        case BufferType::Vertex:
            return D3D11_BIND_VERTEX_BUFFER;
        case BufferType::Index:
            return D3D11_BIND_INDEX_BUFFER;
        default:
            assert(!"Unknown buffer type");
            return D3D11_BIND_VERTEX_BUFFER;
    }
}

HRESULT DynamicBuffer::init(ID3D11Device* device, UINT size, BufferType type) {
    D3D11_BUFFER_DESC desc{};
    desc.ByteWidth      = size;
    desc.Usage          = D3D11_USAGE_DYNAMIC;
    desc.BindFlags      = typeToBindFlag(type);
    desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

    if (FAILED(device->CreateBuffer(&desc, nullptr, &buffer_)))
        return E_FAIL;
    return S_OK;
}

void DynamicBuffer::free() {
    if (buffer_)
        buffer_->Release();
}

void* DynamicBuffer::map(ID3D11DeviceContext* context, D3D11_MAP type) {
    assert(buffer_ && "Buffer must not be null, init not called/successful or Dynamic buffer freed already");
    if (!buffer_)
        return nullptr;
    
    D3D11_MAPPED_SUBRESOURCE mapped;
    if (FAILED(context->Map(buffer_, 0, type, 0, &mapped)))
        return nullptr;

    return mapped.pData;
}

void DynamicBuffer::unmap(ID3D11DeviceContext* context) {
    assert(buffer_ && "Buffer must not be null, init not called/successful or Dynamic buffer freed already");
    if (!buffer_)
        return;

    context->Unmap(buffer_, 0);
}

ID3D11Buffer* DynamicBuffer::getBuffer() const {
    return buffer_;
}

DynamicBuffer::~DynamicBuffer() {
    free();
}

