#include "VertexCache.h"

#include "Logging.h"
#include "Util.h"

HRESULT VertexCache::init(ID3D11Device* device, UINT size) {
    if (FAILED(buffer_.init(device, size, BufferType::Vertex)))
        return E_FAIL;

    size_ = size;
    bytesUsed_ = 0;

    return S_OK;
}

void* VertexCache::mapVerts(ID3D11DeviceContext* context, UINT numVert, UINT stride, ID3D11Buffer** buffer, UINT& outOffset) {
    const UINT bytes = numVert * stride;
    if (bytes > size_) {
        ex::log(ex::LogLevel::Error, "VertexCache::getVerts cache not large enough, size: %d, bytes requested: %d", size_, bytes);
        return nullptr;
    }

    bytesUsed_ = Util::align(bytesUsed_, stride);

    *buffer = buffer_.getBuffer();

    void* ptr{};

    // If there is not enough space in the current buffer
    if (bytesUsed_ + bytes > size_)
    {
        outOffset = 0;
        bytesUsed_ = bytes;
        // Discard means that this buffer gets new underlying memory completely uninitialized
        // the DX runtime manages deallocation of the old one when not needed anymore.
        // This way we need to manage just a single buffer and easily suballocate from it
        // and we don't need to worry about running out of space.
        ptr = buffer_.map(context, D3D11_MAP_WRITE_DISCARD);
    }
    else // There is enough space in the current buffer
    {
        outOffset = bytesUsed_;
        bytesUsed_ += bytes;
        // NO_OVERWRITE tells the runtime we will not write to any currently used memory
        // this means that the same buffer may currently read in the IA stage and we can
        // write to its uninitialized parts.
        ptr = buffer_.map(context, D3D11_MAP_WRITE_NO_OVERWRITE);
    }

    ptr = (unsigned char*)ptr + outOffset;
    return ptr;
}

void VertexCache::unmapVerts(ID3D11DeviceContext* context) {
    buffer_.unmap(context);
}

