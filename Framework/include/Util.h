#pragma once

namespace Util {

inline float lerp(const float a, const float b, const float t) {
    return a + t * (b - a);
}

inline unsigned int align(unsigned int addr, unsigned int alignment) {
    assert(alignment && "Alignment must not be zero");
    return ((addr + alignment - 1) / alignment) * alignment;
}

}
