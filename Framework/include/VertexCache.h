#pragma once

#include "DynamicBuffer.h"

class VertexCache {
public:
    HRESULT init(ID3D11Device* device, UINT size);
    void* mapVerts(ID3D11DeviceContext* context, UINT numVerts, UINT alignment, ID3D11Buffer** buffer, UINT& outOffset);
    void unmapVerts(ID3D11DeviceContext* context);

private:
    DynamicBuffer buffer_{};
    UINT size_{};
    UINT bytesUsed_{};
};
