#pragma once

#include "ex_d3d11.h"

enum class BufferType {
    Vertex,
    Index
};

class DynamicBuffer {
public:
    DynamicBuffer() = default;
    ~DynamicBuffer();

    HRESULT init(ID3D11Device* device, UINT size, BufferType type);
    void free();

    void* map(ID3D11DeviceContext* context, D3D11_MAP type);
    void unmap(ID3D11DeviceContext* context);

    ID3D11Buffer* getBuffer() const;

private:
    ID3D11Buffer* buffer_{};
};

